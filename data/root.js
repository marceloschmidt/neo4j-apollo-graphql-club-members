import Person from '../neo4j/person';
import Club from '../neo4j/club';
import Store from '../neo4j/store';


const resolve = {
	RootQuery: {
		people(_, { limit }) {
			return Person.findAll({ limit: limit })
		},
		person(_, { name }) {
			return Person.findByName(name);
		},
		club(_, { title }) {
			const club = new Club;
			return club.findByTitle(title);
		},
		store(_, { name }) {
			const store = new Store;
			return store.findByName(name);
		},
	}
};

export default resolve;
