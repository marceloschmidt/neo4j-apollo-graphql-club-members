const resolve = {
	Store: {
		clubs(store, options) {
			return store.getClubs(options);
		},
	}
};

export default resolve;
