import _ from 'underscore';
import rootResolver from './root';
import personResolver from './person';
import clubResolver from './club';
import storeResolver from './store';

export default _.extend({}, rootResolver, personResolver, clubResolver, storeResolver);
