const resolve = {
	Person: {
		clubs(person) {
			return person.getClubs();
		},
		friends(person) {
			return person.getFriends();
		},
		discountStores(person) {
			return person.getStores();
		}
	}
};

export default resolve;
