const resolve = {
	Club: {
		members(club) {
			return club.getMembers();
		},
		discountStores(club) {
			return club.getStores();
		},
	}
};

export default resolve;
