'use strict';
import casual from 'casual';
import _ from 'underscore';
import neo4j from './neo4j/db';

const num_people = 10;
const num_clubs = 5;

let ps = [];
let cs = [];
for (let n = 0; n < num_people; n++) {
	let key = 'P' + n;
	console.log(`CREATE (${key}:Person {name: '${casual.full_name}', born: ${casual.year}})`);
	ps.push(key);
}
for (let n = 0; n < num_clubs; n++) {
	let key = 'C' + n;
	console.log(`CREATE (${key}:Club {title: '${casual.title}', established: ${casual.year}})`);
	cs.push(key);
}
for (let p1 of ps) {
	for (let p2 of ps) {
		if (p1 === p2) {
			continue;
		}
		if (Math.random() > 0.8) {
			console.log(`CREATE (${p1})-[:IS_FRIEND_OF]->(${p2})`);
		}
	}
	for (let c of cs) {
		if (Math.random() > 0.5) {
			console.log(`CREATE (${p1})-[:BELONGS_TO]->(${c})`);
		}
	}
}
