import neo4j from 'neo4j';
import bluebird from 'bluebird';

const neo4jDB = new neo4j.GraphDatabase('http://neo4j:senha@192.168.99.100:7474');
bluebird.promisifyAll(neo4jDB);

export default neo4jDB;
