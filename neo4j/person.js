import _ from 'underscore';
import neo4j from './db';
import Club from './club';
import Store from './store';

class Person {
	constructor(data) {
		if (data) {
			if (data.name) {
				this.name = data.name;
			}
			if (data.born) {
				this.born = data.born;
			}
		}
	}

	static findAll(options) {
		if (options.limit) {
			let limit = Math.min(options.limit, 50);
			return neo4j.cypherAsync({
				query: 'MATCH (person:Person) RETURN person LIMIT {limit}',
				params: {
					limit: limit
				}
			}).then((results) => {
				if (!results || results.length === 0) {
					return null;
				} else {
					let people = [];
					results.forEach(result => {
						people.push(new Person(result['person'].properties));
					})
					return people;
				}
			}).catch(error => {
				throw error;
			});
		} else {
			return null;
		}
	}

	static findByName(name) {
		return neo4j.cypherAsync({
			query: 'MATCH (person:Person {name: {name}}) RETURN person',
			params: {
				name: name
			}
		}).then((results) => {
			let result = results[0];
			if (!result) {
				return null;
			} else {
				let person = new Person;
				person.name = result['person'].properties.name;
				person.born = result['person'].properties.born;
				return person;
			}
		}).catch(error => {
			throw error;
		});
	}

	getClubs() {
		return neo4j.cypherAsync({
			query: 'MATCH (:Person {name: {name}})-[:BELONGS_TO]->(club:Club) RETURN club',
			params: {
				name: this.name
			}
		}).then((results) => {
			if (!results || results.length === 0) {
				return null;
			} else {
				let clubs = [];
				results.forEach(result => {
					clubs.push(new Club(result['club'].properties));
				})
				return clubs;
			}
		}).catch(error => {
			throw error;
		});
	}

	getFriends() {
		return neo4j.cypherAsync({
			query: 'MATCH (:Person {name: {name}})-[:IS_FRIEND_OF]->(person:Person) RETURN person',
			params: {
				name: this.name
			}
		}).then((results) => {
			if (!results || results.length === 0) {
				return null;
			} else {
				let friends = [];
				results.forEach(result => {
					friends.push(new Person(result['person'].properties));
				})
				return friends;
			}
		}).catch(error => {
			throw error;
		});
	}

	getStores() {
		return this.getClubs()
		.then(clubs => {
			let promises = [];
			for (let club of clubs) {
				promises.push(new Promise((resolve, reject) => {
					return neo4j.cypherAsync({
						query: 'MATCH (:Club {title: {title}})-[discount:OFFERS_DISCOUNT_AT]->(store:Store) RETURN discount, store',
						params: {
							title: club.title
						}
					}).then((results) => {
						if (!results || results.length === 0) {
							return resolve(null);
						} else {
							let stores = [];
							results.forEach(result => {
								stores.push(new Store(_.extend(result['store'].properties, result['discount'].properties)));
							})
							return resolve(stores);
						}
					}).catch(error => {
						reject(error);
					});
				}));
			}

			return Promise.all(promises).then(values => {
				return Store.unique(values);
			})
		})
	}
}

export default Person;
