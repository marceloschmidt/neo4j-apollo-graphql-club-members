import _ from 'underscore';
import neo4j from './db';
import Person from './person';
import Store from './store';

class Club {
	constructor(data) {
		if (data) {
			if (data.title) {
				this.title = data.title;
			}
			if (data.established) {
				this.established = data.established;
			}
			if (data.discount) {
				this.discount = data.discount;
			}
		}
	}

	static findByTitle(title) {
		return neo4j.cypherAsync({
			query: 'MATCH (club:Club {title: {title}}) RETURN club',
			params: {
				title: title
			}
		}).then((results) => {
			let result = results[0];
			if (!result) {
				return null;
			} else {
				let club = new Club;
				club.title = result['club'].properties.title;
				club.established = result['club'].properties.established;
				return club;
			}
		}).catch(error => {
			throw error;
		});
	}

	getMembers() {
		return neo4j.cypherAsync({
			query: 'MATCH (:Club {title: {title}})<-[:BELONGS_TO]-(person:Person) RETURN person',
			params: {
				title: this.title
			}
		}).then((results) => {
			if (!results || results.length === 0) {
				return null;
			} else {
				let members = [];
				results.forEach(result => {
					members.push(new Person(result['person'].properties));
				})
				return members;
			}
		}).catch(error => {
			throw error;
		});
	}

	getStores() {
		return neo4j.cypherAsync({
			query: 'MATCH (:Club {title: {title}})-[discount:OFFERS_DISCOUNT_AT]->(store:Store) RETURN discount, store',
			params: {
				title: this.title
			}
		}).then((results) => {
			if (!results || results.length === 0) {
				return null;
			} else {
				let stores = [];
				results.forEach(result => {
					stores.push(new Store(_.extend(result['store'].properties, result['discount'].properties)));
				})
				console.log(stores);
				return stores;
			}
		}).catch(error => {
			throw error;
		});
	}
}

export default Club;
