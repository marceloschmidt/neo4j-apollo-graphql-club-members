import _ from 'underscore';
import neo4j from './db';
import Club from './club';

class Store {
	constructor(data) {
		if (data) {
			if (data.name) {
				this.name = data.name;
			}
			if (data.discount) {
				this.discount = data.discount;
			}
		}
	}

	static unique(values) {
		values = _.flatten(values);
		let names = [];
		for (let i in values) {
			if (names.indexOf(values[i].name) !== -1) {
				delete values[i];
			} else {
				names.push(values[i].name);
			}
		}
		return _.compact(values);
	}

	findByName(name) {
		return neo4j.cypherAsync({
			query: 'MATCH (store:Store {name: {name}}) RETURN store',
			params: {
				name: name
			}
		}).then((results) => {
			let result = results[0];
			if (!result) {
				return null;
			} else {
				this.name = result['store'].properties.name;
				return this;
			}
		}).catch(error => {
			throw error;
		});
	}

	getClubs({ minDiscount }) {
		return neo4j.cypherAsync({
			query: 'MATCH (:Store {name: {name}})<-[discount:OFFERS_DISCOUNT_AT]-(club:Club) WHERE discount.discount >= {minDiscount} RETURN discount, club',
			params: {
				name: this.name,
				minDiscount: minDiscount || 0,
			}
		}).then((results) => {
			if (!results || results.length === 0) {
				return null;
			} else {
				let clubs = [];
				results.forEach(result => {
					clubs.push(new Club(_.extend(result['club'].properties, result['discount'].properties)));
				})
				return clubs;
			}
		}).catch(error => {
			throw error;
		});
	}
}

export default Store;
