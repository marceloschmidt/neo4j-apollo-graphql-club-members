# neo4j-apollo-graphql-club-members
Sample Neo4j + Apollo + GraphQL application

# Setup
Create initial database with the following Cypher Queries
```
CREATE (P0:Person {name: 'Chad Johnson', born: 1985})
CREATE (P1:Person {name: 'Ursula Ebert', born: 1979})
CREATE (P2:Person {name: 'Denis Bauch', born: 1980})
CREATE (P3:Person {name: 'Nona Davis', born: 1995})
CREATE (P4:Person {name: 'Adrienne Ziemann', born: 1970})
CREATE (P5:Person {name: 'Mariana Miller', born: 1973})
CREATE (P6:Person {name: 'Shayne Gaylord', born: 1976})
CREATE (P7:Person {name: 'Esmeralda Halvorson', born: 1976})

CREATE (C0:Club {title: 'Best Brands', established: 2007})
CREATE (C1:Club {title: 'Clothing Club', established: 1999})
CREATE (C2:Club {title: 'Urban Discounts', established: 1950})
CREATE (C3:Club {title: 'Hartmann', established: 1983})

CREATE (S0:Store {name: 'Travel Cotton'})
CREATE (S1:Store {name: 'Quick Fur'})
CREATE (S2:Store {name: 'I Heart Mac'})
CREATE (S3:Store {name: 'Mike'})
CREATE (S4:Store {name: 'Pluma'})
CREATE (S5:Store {name: 'Redbook'})

CREATE (P0)-[:IS_FRIEND_OF]->(P1)
CREATE (P0)<-[:IS_FRIEND_OF]-(P1)
CREATE (P0)-[:IS_FRIEND_OF]->(P2)
CREATE (P0)<-[:IS_FRIEND_OF]-(P2)
CREATE (P1)-[:IS_FRIEND_OF]->(P3)
CREATE (P1)<-[:IS_FRIEND_OF]-(P3)
CREATE (P2)-[:IS_FRIEND_OF]->(P3)
CREATE (P2)<-[:IS_FRIEND_OF]-(P3)
CREATE (P4)-[:IS_FRIEND_OF]->(P5)
CREATE (P4)<-[:IS_FRIEND_OF]-(P5)
CREATE (P5)-[:IS_FRIEND_OF]->(P6)
CREATE (P5)<-[:IS_FRIEND_OF]-(P6)

CREATE (P0)-[:BELONGS_TO]->(C0)
CREATE (P1)-[:BELONGS_TO]->(C0)
CREATE (P1)-[:BELONGS_TO]->(C1)
CREATE (P2)-[:BELONGS_TO]->(C1)
CREATE (P2)-[:BELONGS_TO]->(C2)
CREATE (P3)-[:BELONGS_TO]->(C0)
CREATE (P3)-[:BELONGS_TO]->(C1)
CREATE (P3)-[:BELONGS_TO]->(C2)
CREATE (P3)-[:BELONGS_TO]->(C3)
CREATE (P4)-[:BELONGS_TO]->(C1)
CREATE (P4)-[:BELONGS_TO]->(C3)
CREATE (P5)-[:BELONGS_TO]->(C3)
CREATE (P6)-[:BELONGS_TO]->(C0)
CREATE (P7)-[:BELONGS_TO]->(C0)
CREATE (P7)-[:BELONGS_TO]->(C2)

CREATE (C0)-[:OFFERS_DISCOUNT_AT { discount: 10 }]->(S3)
CREATE (C0)-[:OFFERS_DISCOUNT_AT { discount: 5 }]->(S4)
CREATE (C0)-[:OFFERS_DISCOUNT_AT { discount: 7 }]->(S5)
CREATE (C1)-[:OFFERS_DISCOUNT_AT { discount: 7 }]->(S0)
CREATE (C1)-[:OFFERS_DISCOUNT_AT { discount: 15 }]->(S1)
CREATE (C1)-[:OFFERS_DISCOUNT_AT { discount: 7.5 }]->(S4)
CREATE (C2)-[:OFFERS_DISCOUNT_AT { discount: 4 }]->(S2)
CREATE (C3)-[:OFFERS_DISCOUNT_AT { discount: 5 }]->(S2)
CREATE (C3)-[:OFFERS_DISCOUNT_AT { discount: 5 }]->(S3)
CREATE (C3)-[:OFFERS_DISCOUNT_AT { discount: 5 }]->(S5)
```

# Where can "Adrienne Ziemann" get discounts at, and how much?
```
query {
  person(name:"Adrienne Ziemann") {
    discountStores {
      name
      discount
    }
  }
}
```

# What stores does the club "Best Brands" offer discounts at, and how much?
```
query {
  club(title:"Best Brands") {
    title
    established
    discountStores {
      name
      discount
    }
  }
}
```

# What are the clubs that offer at least 7.5 percent discount at "Mike"?
```
query {
  store(name:"Mike") {
    clubs(minDiscount: 7.5) {
      title
      discount
    }
  }
}
```
