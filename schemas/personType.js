import {
	GraphQLObjectType,
	GraphQLList,
	GraphQLNonNull,
	GraphQLString,
	GraphQLInt,
} from 'graphql';

import clubType from './clubType';
import storeType from './storeType';

var personType = new GraphQLObjectType({
	name: 'Person',
	fields: () => ({
		name: {
			type: new GraphQLNonNull(GraphQLString)
		},
		born: {
			type: GraphQLInt
		},
		clubs: {
			type: new GraphQLList(clubType)
		},
		friends: {
			type: new GraphQLList(personType)
		},
		discountStores: {
			type: new GraphQLList(storeType)
		}
	})
});

export default personType;
