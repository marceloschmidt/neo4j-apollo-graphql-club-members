import {
	GraphQLObjectType,
	GraphQLList,
	GraphQLNonNull,
	GraphQLString,
	GraphQLInt,
	GraphQLFloat,
} from 'graphql';

import clubType from './clubType';

var storeType = new GraphQLObjectType({
	name: 'Store',
	fields: () => ({
		name: {
			type: new GraphQLNonNull(GraphQLString)
		},
		discount: {
			type: GraphQLFloat
		},
		clubs: {
			type: new GraphQLList(clubType),
			args: {
				minDiscount: {
					type: GraphQLFloat
				}
			}
		}
	})
});

export default storeType;
