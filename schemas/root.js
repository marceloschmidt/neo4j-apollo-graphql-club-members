import {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLList,
	GraphQLNonNull,
	GraphQLString,
	GraphQLInt,
	GraphQLID,
} from 'graphql';

import personType from './personType';
import clubType from './clubType';
import storeType from './storeType';

var query = new GraphQLObjectType({
	name: 'RootQuery',
	fields: {
		people: {
			type: new GraphQLList(personType),
			args: {
				limit: {
					type: GraphQLInt
				}
			}
		},
		person: {
			type: personType,
			args: {
				name: {
					type: GraphQLString
				},
				born: {
					type: GraphQLInt
				},
			}
		},
		club: {
			type: clubType,
			args: {
				title: {
					type: new GraphQLNonNull(GraphQLString)
				},
				established: {
					type: GraphQLInt
				},
			}
		},
		store: {
			type: storeType,
			args: {
				name: {
					type: new GraphQLNonNull(GraphQLString)
				}
			}
		},
	}
});

var schema = new GraphQLSchema({
	query: query
});

export default schema;
