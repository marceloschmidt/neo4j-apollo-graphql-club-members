import {
	GraphQLObjectType,
	GraphQLList,
	GraphQLNonNull,
	GraphQLString,
	GraphQLInt,
	GraphQLFloat,
} from 'graphql';

import personType from './personType';
import storeType from './storeType';

var clubType = new GraphQLObjectType({
	name: 'Club',
	fields: () => ({
		title: {
			type: new GraphQLNonNull(GraphQLString)
		},
		established: {
			type: GraphQLInt
		},
		discount: {
			type: GraphQLFloat
		},
		members: {
			type: new GraphQLList(personType)
		},
		discountStores: {
			type: new GraphQLList(storeType)
		}
	})
});

export default clubType;
