import express from 'express';
import { apolloServer, addResolveFunctionsToSchema } from 'graphql-tools';
import schema from './schemas/root';
import Resolvers from './data/resolvers';

const GRAPHQL_PORT = 8080;

const graphQLServer = express();
graphQLServer.use('/graphql', apolloServer({
  graphiql: true,
  pretty: true,
  schema: schema,
}));

addResolveFunctionsToSchema(schema, Resolvers);

graphQLServer.listen(GRAPHQL_PORT, () => console.log(
  `GraphQL Server is now running on http://localhost:${GRAPHQL_PORT}/graphql`
));
